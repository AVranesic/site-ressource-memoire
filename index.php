<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ana-Ressource</title>
    <meta name="description" content="An interactive getting started guide for Brackets.">
    <link rel="stylesheet" href="css/style.css">

</head>

<body>

    <!-----------------  FORMULAIRE--------------------->
<!--
    <p>
        HOLA Veuillez entrer votre nom :
    </p>

    <form action="cible.php" method="post">
        <p>
            <input type="text" name="prenom" />
            <input type="submit" value="Valider" />
        </p>
        <select name="choix">
    <option value="choix1">Bonjour</option>
    <option value="choix2">Aurevoir</option>
</select> Formulaire d'envoi de fichier :<br />
        <input type="file" name="monfichier" /><br />
        <input type="submit" value="Envoyer le fichier" />
    </form>
-->


    <!-----------------  CONTACT --------------------->
    <div class="contact">
        <p class="contact">
            -----------------------
            <br>Ana Vranesic
            <br>vranesic.anais@gmail.com
            <br> Master ESAC Cambrai

            <br> ❑ <a href="https://www.instagram.com/ana.vranesic/" target="_blank">Instagram</a> ❑ <a href="http://anaisvranesic.fr/" target="_blank">Site</a></p>
    </div>

    <!-----------------  MENU --------------------->
    <h2 class="page">
        <span class="propos">GRAPHISME ET ACTIVISME</span>
        <br><span class="entretien">ENTRETIENS</span>
        <br><span class="etudes">ÉTUDES DE CAS</span>
        <br><span class="livres">BIBLIOGRAPHIE</span></h2>


    <!-----------------  À PROPOS --------------------->
    <div class="resume">
        <p class="explication">
            Bienvenue
            <br> Étudiante en 4<sup>e</sup> année de Design Graphique à l'ESAC de Cambrai, j'ai décidé de créer un site ressource dédié à mon sujet de mémoire traitant du graphisme au travers de l'activisme de la lutte contre le sida. Au travers de ce site, plusieurs fonctionnalités s'offre à vous :</p>
            <ul> Des Tags</ul>
            <ul> Des interviews</ul>
            <ul> Des études de cas</ul>
            <ul> Une bibliographie</ul>
            <ul> Un pad</ul>
            <p class="explication">
            Le mémoire sera disponible et téléchargeable en 2019 en .PDF
            <br>
            <br>
            <u>Remerciement :</u> 
            <br>Studio Luuse
            <br>Caroll Maréchal
            <br> Matthias Sweizer
        </p>
    </div>

    <!-----------------  BIBLIOGRAPHIE --------------------->
    <div class="bibliographie">
        <ul>
            <h3>BIBLIOGRAPHIE</h3>
        </ul>
        <!--        <marquee class="marqueebibli"> Si vous avez des livres à me conseiller, vous pouvez les inscrire dans le pad à votre disposition sur la page d'accueil.</marquee>-->
        <ul><a href="http://www.lespressesdureel.com/ouvrage.php?id=5483" target="_blank"><u>Ce que le sida m'a fait</u>, Lebovici Élisabeth (2017)</a></ul>
        <ul><a href="http://www.denoel.fr/Catalogue/DENOEL/Impacts/Act-Up2" target="_blank"><u>Act-Up, une histoire</u>, Didier Lestrade (..)</a></ul>
        <ul><a href="https://www.amazon.fr/Act-Up-Paris-Action-vie/dp/2360000020"><u>Action</u></a></ul>
        <ul><a href="https://www.cairn.info/revue-multitudes-2004-1-page-39.htm" target="_blank"><u>Détournement de l'art et des médias pour une efficacité politique</u></a></ul>
        <ul><a href="https://www.cairn.info/revue-multitudes-2001-2-page-125.htm" target="_blank"><u>Des génériques à tout prix</u></a></ul>
        <ul><a href="https://www.cairn.info/revue-francaise-de-science-politique-2005-5-page-787.htm" target="_blank"><u>Lutte contre le sida : la France doit mieux faire</u></a></ul>
        <ul><a href="https://www.cairn.info/revue-ethnologie-francaise-2011-1-page-79.html" target="_blank"><u>Les limites de l'approche publicitaire dans la prévention</u></a></ul>
        <ul><a href="https://www.cairn.info/revue-hermes-la-revue-2005-1-page-103.htm" target="_blank"><u>Les associations de lutte contre le sida et la communication publique : une influence minoritaire</u></a></ul>
        <ul><a href="https://www.publishersweekly.com/978-0-941920-16-2" target="_blank"><u>AIDS Demo Graphics</u></a></ul>
        <ul><a href="  https://www.decitre.fr/livres/a-l-ami-qui-ne-m-a-pas-sauve-la-vie-9782070385034.html" target="_blank"><u>À l'ami qui ne m'a pas sauvé la vie</u></a></ul>
        <ul><a href="https://www.cambridge.org/core/books/life-and-death-of-act-upla/4D36662F87D63815D9C796DBC8018832" target="_blank"><u>The Life and Death of Act-Up/LA</u>, Benita Roth</a></ul>
        <ul><a href="http://press.uchicago.edu/ucp/books/book/chicago/M/bo6943529.html" target="_blank"><u>Moving Politics - Emotion and act-up's fight against aids</u>, Deborah B. Gould</a></ul>

    </div>
    <!-----------------  ANALYSE DE CAS --------------------->
    <div class="analyse">
        <h3 class="pastille">n°1</h3>
        <h3>
            <br>Affiche ACT-UP PARIS,
            <br>1<sup>e</sup> decembre 1994,
            <br>MOBILISATION GENERALE</h3>
        <p class="explication"></p>
        <!--        <img src="img/mobilisation.jpg">-->
    </div>

    <!-----------------  INTERVIEW --------------------->
    <div class="inter">
        <p class="explication">
            <!--            <marquee class="marqueeexpli"> À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR !À VENIR ! </marquee>-->
        </p>
    </div>

    <!-----------------  TAG --------------------->
    <div class="filters">

        <div class="ui-group">
            <div class="button-group js-radio-button-group" data-filter-group="tag">

                <!--                                    <button class="button is-checked" data-filter="">#TAGS</button>-->

                <br>
                <button class="button" data-filter=".benetton">#Benneton</button>
                <button class="button" data-filter=".sida">#Sida</button>
                <button class="button" data-filter=".pub">#Publicité</button>
                <button class="button" data-filter=".act-up">#Act-up</button>
                <button class="button" data-filter=".marque">#Marque</button>
                <button class="button" data-filter=".justice">#Justice</button>
                <button class="button" data-filter=".politique">#Politique</button>
                <button class="button" data-filter=".affiche">#Affiche</button>
                <button class="button" data-filter=".censure">#Censure</button>
                <button class="button" data-filter=".revue">#Revue</button>
                <button class="button" data-filter=".action">#Action</button>
                <button class="button" data-filter=".periodique">#Périodique</button>
                <button class="button" data-filter=".archive">#Archive</button>
                <button class="button" data-filter=".video">#Video</button>
                <button class="button" data-filter=".campagne">#Campagne</button>
                <button class="button" data-filter=".lutte">#Lutte</button>
                <button class="button" data-filter=".120BPM">#120BPM</button>
                <button class="button" data-filter=".graphisme">#Graphisme</button>
                <button class="button" data-filter=".cairn">#Cairn</button>
                <button class="button" data-filter=".crips">#Crips</button>
                <button class="button" data-filter=".prevention">#Prévention</button>
                <button class="button" data-filter=".pdf">#PDF</button>
                <button class="button" data-filter=".exposition">#Exposition</button>
                <button class="button" data-filter=".mai68">#Mai68</button>
                <button class="button" data-filter=".usbeketrica">#UsbeketRica</button>
                <button class="button" data-filter=".podcast">#Podcast</button>
                <button class="button" data-filter=".franceculture">#Franceculture</button>
                <button class="button" data-filter=".lebovici">#ÉlisabethLebovici</button>
                <button class="button" data-filter=".sidaction">#Sidaction</button>
                <button class="button" data-filter=".amj">#AnnaMonikaJost</button>
                <button class="button" data-filter=".elcs">#ELCS</button>
                <button class="button" data-filter=".hovhannessian">#LucieHovhannessian</button>
                <button class="button" data-filter=".temoignage">#Témoignage</button>
                <button class="button" data-filter=".aides">#Aides</button>
                <button class="button" data-filter=".fiercepussy">#Fiercepussy</button>
                <button class="button" data-filter=".opensource">#OpenSource</button>
                <button class="button" data-filter=".zap">#Zap</button>
                <button class="button" data-filter=".deletegrindr">#Deletegrindr</button>
                <button class="button" data-filter=".boycott">#Boycott</button>
                <button class="button" data-filter=".demission">#Démission</button>
                <button class="button" data-filter=".jeunes">#Jeunes</button>
                <button class="button" data-filter=".interview">#Interview</button>
                <button class="button" data-filter=".philippemangeot">#PhilippeMangeot</button>
                <button class="button" data-filter=".culture">#Culture</button>
                <button class="button" data-filter=".biographie">#Biographie</button>
                <button class="button" data-filter=".christophemartet">#ChristopheMartet</button>
                <button class="button" data-filter=".herveguilbert">#HervéGuilbert</button>
                <button class="button" data-filter=".cleewsvellay">#CleewsVellay</button>


            </div>
        </div>
    </div>

    <!--        </div>-->

    <div class="grid">
        <!-----------------  MARQUEE --------------------->

        <!--        <marquee> ► HELLO ━ BIENVENUE ━ Ici, se regroupe plusieurs articles, vidéos, livres sur l'activisme au travers de la lutte contre le sida. ━ Ce site est classé par tags, vous pouvez grâce aux tags, aller directement à l'essentiel. </marquee>-->
        <!-----------------  PAD --------------------->
        <div class="pad">
            <iframe name='embed_readwrite' src='https://annuel2.framapad.org/p/Ressource-AnaVranesic' width=450 height=400></iframe>
        </div>

        <!-----------------  JQUERY--------------------->
        <?php
    include('functions/parsedown.php');
?>

            <?php
	$urlPad = 'https://mensuel.framapad.org/P/padmemoiretest/export/txt';
	$textPad = file_get_contents($urlPad);    
    $parser = new Parsedown();
    $textHtml = $parser->text($textPad);
    
?>

                <div class="articles">
                    <?php echo $textHtml; ?>
                </div>

    </div>

    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
    <script src='http://npmcdn.com/isotope-layout@3/dist/isotope.pkgd.js'></script>
    <script src="js/index.js"></script>
    <!--    <script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.js"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $(".bibliographie").hide();
            $(".livres").click(function() {
                $(".bibliographie").toggle();
            });
        });

    </script>
    <script>
        $(document).ready(function() {
            $(".analyse").hide();
            $(".etudes").click(function() {
                $(".analyse").toggle();
            });

            $('a').attr('target', '_blank');
        });

    </script>
    <script>
        $(document).ready(function() {
            $(".inter").hide();
            $(".entretien").click(function() {
                $(".inter").toggle();
            });
        });

    </script>

    <script>
        $(document).ready(function() {
            $(".resume").hide();
            $(".propos").click(function() {
                $(".resume").toggle();
            });
        });

    </script>
    <script>
        $('span').click(function() {
            $('span').removeClass('propos');
            $(this).toggleClass('propos');
            $('span').removeClass('entretien');
            $(this).toggleClass('entretien');
            $('span').removeClass('etudes');
            $(this).toggleClass('etudes');
            $('span').removeClass('livres');
            $(this).toggleClass('livres');

        })
        //            
        //            var buttonOrder = $('.button-group').find('.button');
        //            var buttonOrder = buttonOrder.text().sort();
        //            console.log(buttonOrder.text().sort());
        //            buttonOrder.each(function(){
        //                $(this).prependTo('.button-group');
        //            })
        //        });

    </script>

</body>


</html>
